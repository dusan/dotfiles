set scrolloff 10
set hidden true
set ifs "\n"
set autoquit true
set wrapscroll true

set previewer ctpv
set cleaner ctpvclear
&ctpv -s $id
&ctpvquit $id

cmd open ${{
	case $(file --mime-type "$f" -b) in
		image/*) lf -remote "send $id open_images" ;;
		*/x-raw-disk-image) dmenumount "$f" ;;
		*/zip) lf -remote "send $id mount_archive" ;;
		text/*) $EDITOR $fx ;;
		*) for f in $fx; do setsid $OPENER $f >/dev/null 2>&1 & done ;;
	esac
}}

cmd open_images ${{
	devour nsxiv -aiop 2>/dev/null <<< "$fx" |
	(lf -remote "send $id unselect"
	while read -r file; do
		lf -remote "send $id select \"$file\""
		lf -remote "send toggle"
	done)
}}

cmd open_new ${{
	case $(file --mime-type "$f" -b) in
		inode/directory) _START_LFCD=1 setsid alacritty --working-directory "$f" & ;;
		*) setsid alacritty -e xdg-open "$f" & ;;
	esac
}}

cmd mkdir ${{
	printf "Directory Name: "
	read ans
	mkdir $ans
}}

cmd mkfile ${{
	printf "File Name: "
	read ans
	$EDITOR $ans
}}

cmd yank_path ${{
	echo "$fx" | xclip -i -selection clipboard
}}

cmd yank_image ${{
	[ $(mimetype -Mb "$f") = "image/png" ] && \
	xclip -sel c -t "image/png" "$f" || \
	convert "$f" png:- | xclip -select  clipboard -t "image/png"
}}

# define a custom 'delete' command
cmd delete ${{
	printf "$fx\n"
	printf "delete?[y/n]"
	read ans
	if [ $ans = "y" ]; then
		for x in $fx; do
			rm -rf "$x"
		done
	fi
}}

# compress current file or selected files with tar and gunzip
cmd tar ${{
	set -f
	temp=$f.tmp
	mkdir $temp
	cp -r $fx $temp
	tar czf $f.tar.gz $temp
	rm -rf $temp
}}

# compress current file or selected files with zip
cmd zip ${{
	set -f
	temp=$f.tmp
	mkdir $temp
	cp -r $fx $temp
	cd $temp
	zip -r $f.zip .
	rm -rf $temp
}}

cmd mount_archive ${{
	mntdir="$f-archivemount"
	[ ! -d "$mntdir" ] && {
		mkdir "$mntdir"
		archivemount "$f" "$mntdir"
	}
	lf -remote "send $id cd \"$mntdir\""
	lf -remote "send $id reload"
}}

cmd fzf_jump ${{
	old="$f"
	while true; do
		selected=$(ls -a --color=always | tail -n +3 | \
			fzf --ansi --reverse \
			--bind 'esc:execute(echo "")+abort,space:execute(echo {})+abort,right:accept,left:execute(echo ..)+abort' \
			--preview '[ -f {} ] && bat -p --color=always {} --theme "Visual Studio Dark+" --tabs 2 || ls -ahl {}' \
			--preview-window '75%,<90(50%)'
		)
		if [ $? -eq 130 ] && [ "$selected" != ".." ]; then
			[ -z "$selected" ] && selected="$old"
			lf -remote "send $id select \"$selected\""
			exit 0
		else
			if [ -f "$selected" ]; then
				lf -remote "send $id select \"$selected\""
				xdg-open "$selected"
				exit 0
			elif [ -d "$selected" ]; then
				cd "$selected"
				lf -remote "send $id cd \"$selected\""
			fi
		fi
	done
}}

cmd fzf_find_file ${{
	selected="$(rg --files * | fzf)"
	lf -remote "send $id select \"$selected\""
}}

cmd fzf_find_line ${{
	RG_PREFIX="rg --column --line-number --no-heading --color=always --smart-case "
	INITIAL_QUERY="${*:-}"
	selected=$(
		FZF_DEFAULT_COMMAND="$RG_PREFIX $(printf %q "$INITIAL_QUERY")" \
		fzf --ansi --disabled --query "$INITIAL_QUERY" \
		--bind 'alt-f:execute(echo {+})+abort' \
		--bind "change:reload:sleep 0.1; $RG_PREFIX {q} || true" \
		--delimiter : \
		--preview 'bat --color=always {1} --highlight-line {2} --line-range {2}: --theme "Visual Studio Dark+" --tabs 2'
	)
	code=$?
	if [ -n "${selected[0]}" ]; then
		IFS=':' read -ra selected <<< "$selected"
		lf -remote "send $id select \"${selected[0]}\""
		[ $code -ne 130 ] && $EDITOR +"${selected[1]}" "${selected[0]}"
	fi
}}

# Remove some defaults
map m
map o
map d

# Basic Functions
map H top
map L bottom
map <a-f>j :fzf_jump
map <a-f>f :fzf_find_file
map <a-f>l :fzf_find_line
map . set hidden!
map DD delete
map x cut
map Y yank_path
map <a-y> yank_image
map r :rename; reload
map dr &dragon-drop -a -x $fx
map u %du -sh $f
map Q %{{ lf -remote "send $id :cd $OLDPWD; quit" }}

map mf mkfile
map md mkdir
map mu %dmenuumount "$fx"

map ae $ext $f
map at tar
map az zip
map am mount_archive

map R reload
map C clear
map U unselect
map c :clear; unselect

map L $devour alacritty -e lazygit

map spd set nopreview
map spe set preview
map sr1 set ratios 1
map sr2 set ratios 1:2
map sr3 set ratios 1:2:3
map sr4 set ratios 1:2:3:5

# use enter for shell commands
map <enter> shell

# dedicated keys for file opener actions
map o &xdg-open "$f"
map O $mimeopen --ask $f 2> /dev/null
map <c-o> open_new
map <a-o> $sh $f &

# Movement
map ~ mark-save
map ` mark-load
map gds cd ~/Documents
map gdl cd ~/Downloads
map gl cd ~/.local
map gc cd ~/.config
map gG cd  ~/git
map gpr cd ~/git/5.0_roku/pluto-tv-roku/
map gpo cd ~/git/5.0_roku/pluto-tv-roku/cicd/out
map gpc cd ~/git/5.0_roku/pluto-tv-roku/pluto/components/5.0_app/
map g1 cd ~/hdd_1/
